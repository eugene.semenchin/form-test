// Валидация формы //

;(function() {
	'use strict';

	class Form {
		
		static patternName = /^[а-яёА-ЯЁ\s]+$/;
    	static patternPhone = /^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/;
		static errorMess = [
			'Введите имя', // 0
			'Неверное имя', // 1
      		'Ваш телефон', // 2
      		'Неверный телефон', // 3
		];

		constructor(form) {
			this.form = form;
			this.fields = this.form.querySelectorAll('.form-control');
			this.btn = this.form.querySelector('[type=submit]');
			this.iserror = false;
			this.registerEventsHandler();
		}

		static getElement(el) {
			return el.parentElement.nextElementSibling;
		}

		registerEventsHandler() {
			this.btn.addEventListener('click', this.validForm.bind(this));
			this.form.addEventListener('focus', () => {
				const el = document.activeElement;
				if (el === this.btn) return;
				this.cleanError(el);
			}, true);
			for (let field of this.fields) {
				field.addEventListener('blur', this.validBlurField.bind(this));
			}
		}

		validForm(e) {
			e.preventDefault();
			const formData = new FormData(this.form);
			let error;

			for (let property of formData.keys()) {
				error = this.getError(formData, property);
				if (error.length == 0) continue;
				this.iserror = true;
				this.showError(property, error);
			}

			if (this.iserror) return;
			this.sendFormData(formData);
		}

		validBlurField(e) {
			const target = e.target;
			const property = target.getAttribute('name');
			const value = target.value;
			const formData = new FormData();
			formData.append(property, value);
			const error = this.getError(formData, property);
			if (error.length == 0) return;
			this.showError(property, error);
		}

		getError(formData, property) {
			let error = '';
			const validate = {
				username: () => {
          			if (formData.get('username').length == 0) {
						error = Form.errorMess[0];
					} else if (Form.patternName.test(formData.get('username')) == false) {
						error = Form.errorMess[1];
					}
				},
				
				userphone: () => {
					if (formData.get('userphone').length == 0) {
						error = Form.errorMess[2];
					} else if (Form.patternPhone.test(formData.get('userphone')) == false) {
						error = Form.errorMess[3];
					}
				},
			}

			if (property in validate) {
				validate[property]();
			}
			return error;
		}

		showError(property, error) {
			const el = this.form.querySelector(`[name=${property}]`);
			const errorBox = Form.getElement(el);
			el.classList.add('form-control_error');
			errorBox.innerHTML = error;
			errorBox.style.display = 'block';
		}

		cleanError(el) {
			const errorBox = Form.getElement(el);
			el.classList.remove('form-control_error');
			errorBox.removeAttribute('style');
			this.iserror = false;
		}

		sendFormData(formData) {
			let xhr = new XMLHttpRequest();
			const text__form = document.querySelector('.form__text');
			const list__form = document.querySelector('.form__list');
			const send__form = document.querySelector('.send__form');
			const title__form = document.querySelector('.form__title');
			
			xhr.open('POST', '/sendmail.php', true);
			xhr.onreadystatechange = () => {
				if (xhr.readyState === 4) {
					if (xhr.status === 200) {
						// здесь расположен код вашей callback-функции
						// например, она может выводить сообщение об успешной отправке письма
            
					} else {
						// здесь расположен код вашей callback-функции
						// например, она может выводить сообщение об ошибке
           
					}
				} else {
					// здесь расположен код вашей callback-функции
					// например, она может выводить сообщение об ошибке
				}
			}

			// отправляем данные формы
			xhr.send(formData);
			title__form.classList.toggle('form__title--delete')
			list__form.classList.toggle('form__list--delete');
			send__form.classList.toggle('send__form--active');

			const close__form = document.querySelector('.close__modal'); 
			const vitamed__modal = document.querySelector('.vitamed__modal'); 
			close__form.addEventListener('click', () => {
				vitamed__modal.classList.toggle('vitamed__modal--delete')
			})
		};
	}

	
	const forms = document.querySelectorAll('[name=feedback]');
	if (!forms) return;
	for (let form of forms) {
		const f = new Form(form);
	}
})();